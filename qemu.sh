#!/bin/bash


export QEMU=$(which qemu-system-arm)
export TMP_DIR=$PWD/ESPX-qemu
export RPI_KERNEL=${TMP_DIR}/kernel-qemu-4.14.79-stretch
export RPI_FS=${TMP_DIR}/ESPX-rasp.img
export RPI_TAR=${TMP_DIR}/ESPX-rasp.tar.gz
export PTB_FILE=${TMP_DIR}/versatile-pb.dtb

if [ "$1" == "setup" ]; then

    echo "\nWill setup qemu enviroment in directory:\"$TMP_DIR\""
    read -r -p "Are you sure? [y/N] " response
    if [[ "$response" =~ ^([yY][eE][sS]|[yY])+$ ]]
    then
	mkdir -p $TMP_DIR; cd $TMP_DIR
	echo "\n-> Downloading the Raspberry kernel"
	curl -o ${RPI_KERNEL} -LJ \
	     https://github.com/dhruvvyas90/qemu-rpi-kernel/blob/master/kernel-qemu-4.14.79-stretch?raw=true
	echo "\n-> Downloading Versatile device tree source file"
        curl -o ${PTB_FILE} -LJ \
             https://github.com/dhruvvyas90/qemu-rpi-kernel/blob/master/versatile-pb.dtb?raw=true
	echo "\n-> Downloading the Raspbian <extra>lite version from dropbox"
	curl -o ${RPI_TAR} -LJ \
	    https://www.dropbox.com/s/0sp5a1s6r5ee3kw/ESPX-rasp.tar.gz?dl=1
	echo "\n-> Extracting ${RPI_TAR} into ${RPI_FS}"
	tar xf ${RPI_TAR}
    else
	exit
    fi
    
    
elif [ "$1" = "run" ]; then
	echo RPI_KERNEL: ${RPI_KERNEL}
	echo QEMU: ${QEMU}
	echo PTB_FILE: ${PTB_FILE}
	echo RPI_FS: ${RPI_FS}

#RPI_KERNEL: /mnt/d/Downloads/ESPX-rasp/espx/ESPX-qemu/kernel-qemu-4.14.79-stretch
#QEMU: /usr/bin/qemu-system-arm
#PTB_FILE: /mnt/d/Downloads/ESPX-rasp/espx/ESPX-qemu/versatile-pb.dtb
#RPI_FS: /mnt/d/Downloads/ESPX-rasp/espx/ESPX-qemu/ESPX-rasp.img
	
# qemu-system-arm -kernel ./ESPX-qemu/kernel-qemu-4.14.79-stretch -cpu arm1176 -m 256 -M versatilepb -dtb ./ESPX-qemu/versatile-pb.dtb -no-reboot -serial stdio -append "root=/dev/sda2 panic=1 rootfstype=ext4 rw" -drive "file=./ESPX-qemu/ESPX-rasp.img,index=0,media=disk,format=raw" -net user,hostfwd=tcp::5000-:5000 -net nic

    $QEMU -kernel ${RPI_KERNEL} \
	  -cpu arm1176 -m 256 -M versatilepb \
	  -dtb ${PTB_FILE} -no-reboot \
	  -serial stdio -append "root=/dev/sda2 panic=1 rootfstype=ext4 rw" \
	  -drive "file=${RPI_FS},index=0,media=disk,format=raw" \
	  -net user,hostfwd=tcp::5000-:5000 -net nic
else
    echo "\nUsage of $0 script" \
	 "\n\n-> Run \"$0 setup\" to setup the qemu environment and" \
	 "\ndownload all the necessary files in the current directory." \
	 "\n\n-> Or \"$0 run\" to run qemu\n"
fi
